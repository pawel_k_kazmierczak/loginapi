<?php
return [
    'db' => [
        'database' => 'application_db_test',
        'driver' => 'PDO_Mysql',
        'hostname' => 'localhost',
        'username' => 'root',
        'port' => '3306',
        'dsn' => 'mysql:host=localhost;dbname=application_db_test',
    ],
];
