Requirements
------------
Databases scripts are in folder database

Postman file is in folder postman

Application was tested under php 7.4

Starting dev server

```
php -S 0.0.0.0:8080 -t public public/index.php
```

Running tests (required to copy phpunit.xml.dist to phpunit.xml)

```
./vendor/bin/phpunit
```