<?php
namespace Account\V1\Rpc\CheckUnique;

class CheckUniqueControllerFactory
{

    public function __invoke($container)
    {
        $currencyConverter = $container->get(\Account\Service\UsersService::class);
        return new CheckUniqueController($currencyConverter);
    }
}
