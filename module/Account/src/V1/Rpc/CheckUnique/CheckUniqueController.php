<?php
namespace Account\V1\Rpc\CheckUnique;

use Laminas\Mvc\Controller\AbstractActionController;

class CheckUniqueController extends AbstractActionController
{
    private $userService;

    public function __construct($userService)
    {
        $this->userService = $userService;
    }

    public function checkUniqueAction()
    {
        $email = $this->params()->fromQuery('email');
        return new \Laminas\ApiTools\ContentNegotiation\ViewModel(
            ['is_unique' => !$this->userService->emailExist($email),'email'=>$email]);
    }
}
