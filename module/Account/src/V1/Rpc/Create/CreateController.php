<?php
namespace Account\V1\Rpc\Create;

use Laminas\Mvc\Controller\AbstractActionController;

class CreateController extends AbstractActionController
{
    private $registryService;

    public function __construct($registryService)
    {
        $this->registryService = $registryService;
    }

    public function createAction()
    {
        $request = $this->getRequest();

        $result = $this->registryService->registerUser($request->getPost());

        return new \Laminas\ApiTools\ContentNegotiation\ViewModel($result->toArray());
    }
}
