<?php
namespace Account\V1\Rpc\Create;

class CreateControllerFactory
{
    public function __invoke($container)
    {
        $registryService = $container->get(\Account\Service\RegistryService::class);
        return new CreateController($registryService);
    }
}
