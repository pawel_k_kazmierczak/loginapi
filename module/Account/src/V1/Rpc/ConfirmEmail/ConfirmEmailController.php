<?php
namespace Account\V1\Rpc\ConfirmEmail;

use Laminas\Mvc\Controller\AbstractActionController;

class ConfirmEmailController extends AbstractActionController
{
    public function confirmEmailAction()
    {
        return new \Laminas\ApiTools\ContentNegotiation\ViewModel(['success'=>true]);
    }
}
