<?php
namespace Account;

use Laminas\ApiTools\Provider\ApiToolsProviderInterface;

class Module implements ApiToolsProviderInterface
{
    const USER_GATEWAY = 'UsersGateway';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'Laminas\ApiTools\Autoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src',
                ],
            ],
        ];
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\RegistryService::class => function($sm) {
                    return new Service\RegistryService($sm);
                },
                Service\ActivationEmailSender::class => function($sm) {
                    return new Service\ActivationEmailSender();
                },
                Service\UsersService::class => function($sm) {
                    return new Service\UsersService($sm);
                },
                self::USER_GATEWAY => function ($sm) {
                    $dbAdapter = $sm->get('Laminas\Db\Adapter\Adapter');
                    $resultSetPrototype = new \Laminas\Db\ResultSet\ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Entity\User($sm));
                    return new \Laminas\Db\TableGateway\TableGateway('users', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}
