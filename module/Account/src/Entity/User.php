<?php
namespace Account\Entity;

class User extends \Application\Entity\CommonModel
{
    public $id;
    public $email;
    public $password;
    public $code;
    public $isActive;

    public function exchangeArray($data)
    {
        $this->data = $data;

        $this->id = $this->getValueFromData($data, 'id');
        $this->email = $this->getValueFromData($data, 'email');
        $this->password = $this->getValueFromData($data, 'password');
        $this->code = $this->getValueFromData($data, 'code');
        $this->isActive = $this->getValueFromData($data, 'is_active');
    }

    public function convertToDBName($phpName)
    {
        switch ($phpName) {
            case 'isActive':
                return 'is_active';
            default :
                return $phpName;
        }
    }

    public function zmienHaslo($post)
    {
        if (isset($post['stare_haslo']) && $post['stare_haslo'] != "") {
            $stareHaslo = $this->kodujHaslo($post['stare_haslo']);
            $noweHaslo = $post['nowe_haslo'];
            $noweHaslo2 = $post['nowe_haslo2'];


            $obiektTable = $this->sm->get(\Logowanie\Module::UZYTKOWNIK_TABLE);
            $uzytkownikZDB = $obiektTable->getRow($this->id);

            if ($uzytkownikZDB->haslo !== $stareHaslo) {
                throw new \Exception("Stare hasło nieprawidłowe");
            }

            $this->zmienHasloBezSprawdzaniaStarego($noweHaslo, $noweHaslo2);
        }
    }

    public function zmienHasloBezSprawdzaniaStarego($noweHaslo, $noweHaslo2)
    {
        if ($noweHaslo !== $noweHaslo2) {
            throw new \Exception("Nowe hasła nie są zgodne");
        }

        $this->haslo = $this->kodujHaslo($noweHaslo);
    }

    public function kodujHaslo($hasloJawne)
    {
        return md5($hasloJawne);
    }

    public function __toString()
    {
        if ($this->imie || $this->nazwisko) {
            return $this->imie . ' ' . $this->nazwisko;
        }
        if ($this->mail) {
            return $this->mail;
        }

        return '';
    }

    public function jsonSerialize()
    {

        $this->haslo = '';
        return $this;
    }

    public function czyAktualniePrzegladajacy()
    {

        $zalogowany = $this->getZalogowanego();
        if ($zalogowany) {
            $czyAktualnieZalgowany = $zalogowany->id == $this->id;
            return $czyAktualnieZalgowany;
        }
        return false;
    }

    public function setImieINazwisko($imieINazwisko)
    {
        $this->imie = \Wspolne\Utils\StrUtil::getImie($imieINazwisko);
        $this->nazwisko = \Wspolne\Utils\StrUtil::getNazwisko($imieINazwisko);
    }

    public static function create($sm = null)
    {
        $encja = new Uzytkownik();

        return $encja;
    }
}
