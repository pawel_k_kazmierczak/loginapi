<?php
namespace Account\Service;

class UsersService extends \Application\Service\DBService
{

    public function __construct($sm)
    {
        parent::__construct($sm, \Account\Module::USER_GATEWAY);
    }

    public function emailExist($email)
    {
        return $this->getCount("email='$email'") > 0;
    }
}
