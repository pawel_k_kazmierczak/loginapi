<?php
namespace Account\Service;

use Zend\ServiceManager\ServiceManager;
use \Logowanie\Model\Uzytkownik;
use Pastmo\Wspolne\Exception\PastmoException;

class RegistryService extends \Application\Service\CommonService
{
    private const SALT = ';klajsd;fkl(&#UIDKHLK';

    protected $addedUser;
    protected $result;

    public function __construct(ServiceManager $sm)
    {
        parent::__construct($sm);
    }

    public function registerUser($post)
    {
        try {
            return $this->registerUserTry($post);
        } catch (\Exception $e) {
            $this->result->setError($e->getMessage());
            $this->usersService->rollback();
            return $this->result;
        }
    }

    public function registerUserTry($post)
    {
        $this->usersService->beginTransaction();
        $this->result = \Application\Utils\ActionResponse::create();
        $this->checkEmail($post);

        $this->saveUser($post);
        $this->sendEmail();

        $this->usersService->commit();
        return $this->result;
    }

    protected function akcjePoRejestracji()
    {
        
    }

    public static function makeHashedPassword($password)
    {
        return md5($password . self::SALT);
    }

    public static function createCode($user)
    {
        if (!$user->id) {
            throw new Exception('User not found');
        }
        $result = md5($user->email . random_bytes(10));
        return $result;
    }

    private function checkEmail($dane)
    {
        $mail = $dane['email'];

        if ($this->usersService->emailExist($mail)) {
            throw new \Exception("Your account exist now");
        }
    }

    private function saveUser($post)
    {
        $user = new \Account\Entity\User();

        $user->email = $post->email;
        $user->password = self::makeHashedPassword($post->password);
        $this->usersService->save($user);

        $this->addedUser = $this->usersService->getLastAdded();
    }

    protected function sendEmail()
    {
        $this->addedUser->code = self::createCode($this->addedUser);
        $this->usersService->save($this->addedUser);
        
        $activationEmailSender = $this->get(ActivationEmailSender::class);
        $activationEmailSender->setParameters($this->addedUser->email, $this->addedUser->id, $this->addedUser->code);
        $activationEmailSender->send();
    }
}
