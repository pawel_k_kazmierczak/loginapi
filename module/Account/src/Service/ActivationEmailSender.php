<?php
namespace Account\Service;

use Laminas\Mail;
use Laminas\Mime;

class ActivationEmailSender extends \Application\Service\CommonService
{
    protected $title = "Registration confirm";
    protected $body = "Your accont has ben createn. CLick this link to confirm email: ";
    protected $copyIfNotWork = " (copy it directly to the browser if not work)";
    protected $LINK = '<a href="%s">%s</a>';
    public $id;
    public $code;
    public $mail;
    
    private $content;

    public function send()
    {
        $mail = new Mail\Message();
        $mail->setBody($this->getContent());

        $mail->addTo($this->mail, 'User');
        $mail->setSubject($this->title);

        $transport = $this->getTransport();

        $transport->send($mail);
        
    }

    public function setParameters($mail, $userId, $code)
    {
        $this->mail = $mail;
        $this->id = $userId;
        $this->code = $code;
        $this->content = $this->generateContent();
        $this->tytul = $this->utworzTytul();
    }

    public function utworzTytul()
    {
        return $this->title;
    }

    public function generateContent()
    {
        $result = "";

        $link = "code: {$this->code}";
        $result .= $this->body;
        $result .= sprintf($this->LINK, $link, $link);
        $result .= $this->copyIfNotWork;

        return $result;
    }

    public function generateLink($id, $code)
    {
        $link = $this->generateUrl('account', array('action' => $this->akcja), array(
            'id' => $id, 'code' => $code));
        return $link;
    }

    public function getContent()
    {

        $wysylka = array();

        if ($this->content) {
            $text = new Mime\Part($this->content);
            $text->type = 'text/plain';
            $text->charset = 'utf-8';

            $wysylka[] = $text;
        }

        $mimeMessage = new Mime\Message();
        $mimeMessage->setParts($wysylka);

        return $mimeMessage;
    }

    protected function getTransport()
    {
        return new Mail\Transport\Sendmail();
    }
    
    public function generateUrl($modul, $parametry = array(), $parametryGet = array(), $czyPelnyUrl = false)
    {
        $link = $this->sm->get('ViewHelperManager')->get('url')
            ->__invoke($modul, $parametry, array('force_canonical' => $czyPelnyUrl,
            'query' => $parametryGet));
        return $link;
    }
}
