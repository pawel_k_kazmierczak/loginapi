<?php
return [
    'service_manager' => [
        'factories' => [],
    ],
    'router' => [
        'routes' => [
            'account.rpc.check-unique' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/account/check-unique',
                    'defaults' => [
                        'controller' => 'Account\\V1\\Rpc\\CheckUnique\\Controller',
                        'action' => 'checkUnique',
                    ],
                ],
            ],
            'account.rpc.create' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/account/create',
                    'defaults' => [
                        'controller' => 'Account\\V1\\Rpc\\Create\\Controller',
                        'action' => 'create',
                    ],
                ],
            ],
            'account.rpc.confirm-email' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/account/confirm-email',
                    'defaults' => [
                        'controller' => 'Account\\V1\\Rpc\\ConfirmEmail\\Controller',
                        'action' => 'confirmEmail',
                    ],
                ],
            ],
        ],
    ],
    'api-tools-versioning' => [
        'uri' => [
            0 => 'account.rpc.check-unique',
            1 => 'account.rpc.create',
            2 => 'account.rpc.confirm-email',
        ],
    ],
    'api-tools-rest' => [],
    'api-tools-content-negotiation' => [
        'controllers' => [
            'Account\\V1\\Rpc\\CheckUnique\\Controller' => 'Json',
            'Account\\V1\\Rpc\\Create\\Controller' => 'Json',
            'Account\\V1\\Rpc\\ConfirmEmail\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'Account\\V1\\Rpc\\CheckUnique\\Controller' => [
                0 => 'application/vnd.account.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'Account\\V1\\Rpc\\Create\\Controller' => [
                0 => 'application/vnd.account.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'Account\\V1\\Rpc\\ConfirmEmail\\Controller' => [
                0 => 'application/vnd.account.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
        ],
        'content_type_whitelist' => [
            'Account\\V1\\Rpc\\CheckUnique\\Controller' => [
                0 => 'application/vnd.account.v1+json',
                1 => 'application/json',
            ],
            'Account\\V1\\Rpc\\Create\\Controller' => [
                0 => 'application/vnd.account.v1+json',
                1 => 'application/json',
            ],
            'Account\\V1\\Rpc\\ConfirmEmail\\Controller' => [
                0 => 'application/vnd.account.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'api-tools-hal' => [
        'metadata_map' => [],
    ],
    'api-tools-content-validation' => [
        'Account\\V1\\Rpc\\CheckUnique\\Controller' => [
            'input_filter' => 'Account\\V1\\Rpc\\CheckUnique\\Validator',
        ],
        'Account\\V1\\Rpc\\Create\\Controller' => [
            'input_filter' => 'Account\\V1\\Rpc\\Create\\Validator',
        ],
        'Account\\V1\\Rpc\\ConfirmEmail\\Controller' => [
            'input_filter' => 'Account\\V1\\Rpc\\ConfirmEmail\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'Account\\V1\\Rest\\CheckUnique\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'email',
                'field_type' => 'string',
                'error_message' => 'Email should be correct',
            ],
        ],
        'Account\\V1\\Rpc\\CheckUnique\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\EmailAddress::class,
                        'options' => [
                            'breakchainonfailure' => true,
                        ],
                    ],
                ],
                'filters' => [],
                'name' => 'email',
            ],
        ],
        'Account\\V1\\Rpc\\Create\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\EmailAddress::class,
                        'options' => [],
                    ],
                ],
                'filters' => [],
                'name' => 'email',
                'error_message' => 'Email should be unique and correct formatted',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => '7',
                        ],
                    ],
                ],
                'filters' => [],
                'name' => 'password',
            ],
        ],
        'Account\\V1\\Rpc\\ConfirmEmail\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\EmailAddress::class,
                        'options' => [
                            'breakchainonfailure' => true,
                        ],
                    ],
                ],
                'filters' => [],
                'name' => 'email',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'code',
                'description' => 'One-time code to verify address email',
            ],
        ],
    ],
    'api-tools-mvc-auth' => [
        'authorization' => [],
    ],
    'controllers' => [
        'factories' => [
            'Account\\V1\\Rpc\\CheckUnique\\Controller' => \Account\V1\Rpc\CheckUnique\CheckUniqueControllerFactory::class,
            'Account\\V1\\Rpc\\Create\\Controller' => \Account\V1\Rpc\Create\CreateControllerFactory::class,
            'Account\\V1\\Rpc\\ConfirmEmail\\Controller' => \Account\V1\Rpc\ConfirmEmail\ConfirmEmailControllerFactory::class,
        ],
    ],
    'api-tools-rpc' => [
        'Account\\V1\\Rpc\\CheckUnique\\Controller' => [
            'service_name' => 'CheckUnique',
            'http_methods' => [
                0 => 'GET',
            ],
            'route_name' => 'account.rpc.check-unique',
        ],
        'Account\\V1\\Rpc\\Create\\Controller' => [
            'service_name' => 'Create',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'account.rpc.create',
        ],
        'Account\\V1\\Rpc\\ConfirmEmail\\Controller' => [
            'service_name' => 'ConfirmEmail',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'account.rpc.confirm-email',
        ],
    ],
];
