<?php
return [
    'Account\\V1\\Rest\\CheckUnique\\Controller' => [
        'description' => 'Check if email is unique',
        'entity' => [
            'POST' => [
                'request' => '{
"email":"Adress email to check"
}',
                'response' => '{
"is_unique":""
}',
            ],
        ],
    ],
    'Account\\V1\\Rpc\\CheckUnique\\Controller' => [
        'GET' => [
            'response' => '{
\'is_unique\':\'boolean\'
}',
        ],
    ],
    'Account\\V1\\Rpc\\Create\\Controller' => [
        'POST' => [
            'response' => '{
   "success": "Action result",
   "message": "Info for user"
}',
            'request' => '{
   "email": "",
   "password": ""
}',
        ],
    ],
    'Account\\V1\\Rpc\\ConfirmEmail\\Controller' => [
        'POST' => [
            'request' => '{
   "email": "Address to verify",
   "code": "One-time code to verify address email"
}',
            'response' => '{
    "success": "Operation status",
    "message": "Info for user"
}',
        ],
    ],
];
