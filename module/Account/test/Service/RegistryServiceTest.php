<?php
namespace AccountTest\Service;

use Logowanie\Model\Uzytkownik;
use Pastmo\Testy\Util\TB;

class RegistryServiceTest extends \TestHelpers\CommonDBTest
{
    protected $traceError = true;
    private $registryService;
    private $activationEmailSender;
    private $usersService;

    public function setUp(): void
    {
        parent::setUp();

        $this->activationEmailSender = $this->getMockBuilder(\Account\Service\ActivationEmailSender::class)
            ->getMock();

        $this->overrideService(\Account\Service\ActivationEmailSender::class, $this->activationEmailSender);

        $this->registryService = $this->sm->get(\Account\Service\RegistryService::class);
        $this->usersService = $this->sm->get(\Account\Service\UsersService::class);
    }

    public function test_registerUser()
    {
        $email = 'registerUser' . time();
        $password = 'testPassword';
        
        $result = $this->registryService->registerUser($this->arrayToParameters([
                'email' => $email, 'password' => $password]));

        $this->assertTrue($result->success, $result.'');

        $resultEntity = $this->usersService->getLastAdded();

        $this->assertEquals($resultEntity->email, $email);
        $this->assertEquals($resultEntity->password, \Account\Service\RegistryService::makeHashedPassword($password));
    }
}
