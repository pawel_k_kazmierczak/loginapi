<?php
namespace LogowanieTest\Integracyjne;

class UsersServiceTest extends \TestHelpers\CommonDBTest
{
    protected $traceError = true;
    private $usersService;

    public function setUp(): void
    {
        parent::setUp();

        $this->usersService = $this->sm->get(\Account\Service\UsersService::class);
    }

    public function test_addNewUser()
    {
        $email = 'test_addNewUser' . time();

        $this->assertFalse($this->usersService->emailExist($email));
        $user = new \Account\Entity\User();
        $user->email = $email;
        $user->password = $email;
        $this->usersService->save($user);

        $ostatniRekord = $this->usersService->getLastAdded();

        $this->assertEquals($ostatniRekord->email, $email);
        $this->assertNotNull($ostatniRekord->password);
        $this->assertTrue($this->usersService->emailExist($email), $email);
    }

    public function test_updateUser()
    {
        $email = 'test_addNewUser' . time();
        $email2 = 'test2' . time();

        $user = new \Account\Entity\User();
        $user->email = $email;
        $user->password = $email;
        $this->usersService->save($user);

        $lastValue = $this->usersService->getLastAdded();

        $lastValue->email = $email2;

        $this->usersService->save($lastValue);
        $result = $this->usersService->getRow($lastValue->id);

        $this->assertEquals($result->email, $email2);
    }
}
