<?php
return [
    'Status\\V1\\Rpc\\Ping\\Controller' => [
        'description' => 'Ping the API',
        'GET' => [
            'description' => 'Ping the API for ack',
            'response' => '{
"ack":"Ack request with timestamp"
}',
        ],
    ],
];
