<?php
namespace TestHelpers;

trait TestsParameters
{

    public function arrayToParameters(array $post)
    {
        $parameters = new \Laminas\Stdlib\Parameters($post);
        return $parameters;
    }

    protected function makeObjectMock($klasa, $methods = [])
    {
        return $this->getMockBuilder($klasa)
                ->setConstructorArgs([$this->sm])
                ->setMethods($methods)->getMock();
    }

    protected function overrideService($klucz, $mock)
    {
        $this->sm->setAllowOverride(true);
        $this->sm->setService($klucz, $mock);
    }


}
