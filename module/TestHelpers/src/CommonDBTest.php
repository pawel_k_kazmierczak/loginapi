<?php
namespace TestHelpers;

use Laminas\Stdlib\ArrayUtils;

abstract class CommonDBTest extends \Laminas\Test\PHPUnit\Controller\AbstractHttpControllerTestCase
{

    use TestsParameters;
    protected $sm;
    protected $logowanieFasada;
    public $projekt;
    protected $ustawioneJednorazowo = false;
    protected $zalogowany;
    protected $transakcja = false;

    public function setUp(): void
    {
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
                include __DIR__ . '/../../../config/tests.config.php', ['modules' => include __DIR__ . '/../../../config/modules.config.php'], $configOverrides
        ));

        parent::setUp();

        $this->sm = $this->getApplicationServiceLocator();

//        $this->setDefaultFactoryParameters(PFIntegracyjneZapisNiepowiazane::create($this->sm));


    }

}
