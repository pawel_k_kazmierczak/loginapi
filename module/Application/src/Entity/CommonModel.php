<?php
namespace Application\Entity;

use Pastmo\Wspolne\Utils\EntityUtil;

abstract class CommonModel
{
    public $data;
    protected $doUpdate = false;

    abstract function convertToDBName($nameInClass);

    public function makeArray()
    {
        $class_vars = $this->getClassFields();
        $result = array();
        foreach ($class_vars as $name => $value) {

            $dbName = $this->convertToDBName($name);

            if ($this->$name === null || $this->isIgnored($name)) {
                continue;
            } else if (!is_array($this->$name) && !is_object($this->$name)) {
                $result[$dbName] = $this->$name;
            }
        }
        return $result;
    }

    public function getClassFields()
    {
        return get_class_vars(get_class($this));
    }

    public function isIgnored($name)
    {
        return in_array($name, ["doUpdate", "sm", "data"]);
    }

    public function getValueFromData($data, $key, $defaultValue = null)
    {
        if ($this->doUpdate && isset($this->$key)) {
            $defaultValue = $this->$key;
        } else if ($this->doUpdate) {
            $defaultValue = null;
        }

        return ($this->keyExistInData($data, $key)) ? $data[$key] : $defaultValue;
    }

    private function keyExistInData($data, $key)
    {
        if ($this->doUpdate) {
            return isset($data[$key]);
        }
        return !empty($data[$key]);
    }

    public function updateArray($data)
    {
        $this->doUpdate = true;

        $this->exchangeArray($data);
    }

    public function exchangeArray($data)
    {
        $this->data = $data;
    }

    public function jsonSerialize()
    {
        $result = $this->makeArray();

        return $result;
    }

}
