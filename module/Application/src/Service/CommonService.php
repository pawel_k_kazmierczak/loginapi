<?php
namespace Application\Service;

use Zend\ServiceManager\ServiceManager;
use Zend\EventManager\EventManagerInterface;

class CommonService
{
    use \Application\Utils\ServiceSupply;
    protected $sm;

    public function __construct(ServiceManager $sm = null)
    {
        if ($sm) {
            $this->sm = $sm;
        }
    }

    public function get($serviceName)
    {
        return $this->sm->get($serviceName);
    }

}
