<?php
namespace Application\Service;

use Zend\ServiceManager\ServiceManager;
use Wspolne\Model\FabrykiGetter;

abstract class DBService extends CommonService
{
    protected $tableGateway;
    protected $dbAdapter;

    const DEFAULT_ORDER = "id DESC";
    const CHRONOLOGICZNY_ORDER = "id ASC";
    const DOMYSLNY_LIMIT = false;

    public function __construct(ServiceManager $sm = null, $gatewayName = '')
    {
        parent::__construct($sm);
        if ($this->sm) {
            $this->tableGateway = $this->sm->get($gatewayName);
            $this->dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        }
    }

    public function pobierzResultSetZWherem($where, $order = false, $limit = self::DOMYSLNY_LIMIT, $groupBy = false)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->order($this->getOrder($order));
        $safeWhere = $this->sqlInjectionProtector($where);
        $select->where($safeWhere);
        $this->modyfikujSelecta($select);

        if ($limit) {
            $select->limit($limit);
        }
        if ($groupBy) {
            $select->group($groupBy);
        }

        $string = $this->tableGateway->getSql()->buildSqlString($select);

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function pobierzZWherem($where, $order = false, $limit = false)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->order($this->getOrder($order));
        $safeWhere = $this->sqlInjectionProtector($where);
        $select->where($safeWhere);
        $this->modyfikujSelecta($select);
        if ($limit) {
            $select->limit($limit);
        }

        $string = $this->tableGateway->getSql()->buildSqlString($select);

        $resultSet = $this->wykonajSelect($select);
        return ArrayUtil::konwertujNaArray($resultSet);
    }

    public function wykonajZapytanie($sql)
    {
        $resultSet = $this->tableGateway->adapter->query($sql, 'execute');
        $this->kopiujPrototypDoResultSeta($resultSet);
        return \Pastmo\Wspolne\Utils\ArrayUtil::konwertujNaArray($resultSet);
    }

    public function wykonajZapytanieCount($sql)
    {
        $resultSet = $this->tableGateway->adapter->query($sql, 'execute');
        $current = $resultSet->current();
        return $current->count;
    }

    protected function getOrder($order)
    {
        if ($order) {
            return $order;
        } else {
            return $this->getDomyslnyOrder();
        }
    }

    protected function getDomyslnyOrder()
    {
        return $this->getMainKeyName() . " DESC";
    }

    public function getCount($where = 'true')
    {
        $safeWhere = $this->sqlInjectionProtector($where);

        $table = $this->tableGateway->table;

        $resultSet = $this->tableGateway->getAdapter()->query("select count(*) as result_number FROM $table WHERE $safeWhere;", \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
        $result = $resultSet->current();

        if (isset($result->result_number)) {
            return (int) $result->result_number;
        } else {
            return 0;
        }
    }

    protected function wykonajSelect($select)
    {
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function getRow($id)
    {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getMainKeyName()
    {
        return 'id';
    }

    public function getLastAdded()
    {
        $lastId = $this->tableGateway->getLastInsertValue();
        if ($lastId) {
            $lastRekord = $this->getRow($lastId);
            return $lastRekord;
        } else {
            return NULL;
        }
    }

    public function update($id, $set)
    {
        $this->tableGateway->update($set, array($this->getMainKeyName() => $id));
    }

    public function updateWhere($set, $whereArray)
    {
        $this->tableGateway->update($set, $whereArray);
    }

    public function save(\Application\Entity\CommonModel $model)
    {
        $data = $model->makeArray();

        if ($model->id === null) {
            unset($data['id']);
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRow($data['id'])) {
                $this->tableGateway->update($data, array('id' => $data['id']));
            } else {
                throw new \Exception('Brak rekordu');
            }
        }
    }

    public function delete($where)
    {
        $this->tableGateway->delete($where);
    }

    public function deleteById($id)
    {
        if (!$id || $id === 0) {
            throw new \Exception("Błedny nr id $id");
        }

        $this->delete($this->getMainKeyName() . "=$id");
    }

    public function beginTransaction()
    {
        $this->tableGateway->adapter->getDriver()->getConnection()->beginTransaction();
    }

    public function commit()
    {
        $this->tableGateway->adapter->getDriver()->getConnection()->commit();
    }

    public function rollback()
    {
        $this->tableGateway->adapter->getDriver()->getConnection()->rollback();
    }

    public function extract($object)
    {
        $array = $object->zrobArray();
        return $array;
    }

    public function hydrate(array $data, $object)
    {
        $object->exchangeArray($data);
        return $object;
    }

    protected function kopiujPrototypDoResultSeta($resultSet)
    {
        $resultSet->setArrayObjectPrototype($this->tableGateway->getResultSetPrototype()->getArrayObjectPrototype());
    }

    protected function sqlInjectionProtector($where)
    {

        $pattern = '/(;)/i';
        $replacement = '/;';
        $result = preg_replace($pattern, $replacement, $where);

        return $result;
    }
}
