<?php
namespace Application\Utils;

trait ServiceSupply
{
    protected $menagery = [];

    public function __get($name)
    {
        return $this->getService($name);
    }

    protected function getService($name)
    {
        switch ($name) {
            case 'usersService':
                return $this->get(\Account\Service\UsersService::class);
        }
        $e = new \Exception("No mapping for:" . $name);
        $this->logujWyjatek($e);
        throw $e;
    }
}
