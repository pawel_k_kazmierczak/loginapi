<?php
namespace Application\Utils;

class ActionResponse
{
    public $success = true;
    public $msgs = [];
    protected $data = [];

    public static function create()
    {
        return new static();
    }

    public function toArray()
    {
        return array_merge(['success' => $this->success, 'msg' => implode(',', $this->msgs),], $this->data);
    }

    public function setSuccess($success)
    {
        $this->success = $success;
        return $this;
    }

    public function addMsg($msg)
    {
        $this->msgs[] = $msg;
        return $this;
    }

    public function setError($msg)
    {
        $this->addMsg($msg);
        $this->setSuccess(false);
        return $this;
    }

    public function __toString()
    {
        return implode(",", $this->msgs);
    }

    public function __get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }
}
