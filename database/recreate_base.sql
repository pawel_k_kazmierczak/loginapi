drop database IF EXISTS application_db;
drop database IF EXISTS application_db_test;
CREATE DATABASE `application_db` /*!40100 COLLATE 'utf8_polish_ci' */;
CREATE DATABASE `application_db_test` /*!40100 COLLATE 'utf8_polish_ci' */;